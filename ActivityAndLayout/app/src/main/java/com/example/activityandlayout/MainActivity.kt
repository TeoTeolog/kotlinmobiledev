package com.example.activityandlayout

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Intent(this, SplashActivity::class.java).also{
            startActivity(it)
        }

        var editTextName: EditText = findViewById(R.id.editTextName)
        var editTextLastName: EditText = findViewById(R.id.editTextLastName)
        var editTextAge: EditText = findViewById(R.id.editTextAge)
        var btnBack: Button = findViewById(R.id.btnInfo)
        btnBack.setOnClickListener{
            Intent(this, InfoActivity::class.java).also{
                it.putExtra(Constants.name, editTextName.text.toString())
                it.putExtra(Constants.last_name, editTextLastName.text.toString())
                it.putExtra(Constants.age, editTextAge.text.toString())
                startActivity(it)
            }
        }
    }
}