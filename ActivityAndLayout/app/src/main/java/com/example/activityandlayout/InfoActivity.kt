package com.example.activityandlayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        var btnBack: Button = findViewById(R.id.btnBack)
        btnBack.setOnClickListener{
            finish()
        }

        var text:TextView = findViewById(R.id.textViewInfo)
        val name = intent.getStringExtra(Constants.name)
        val last_name = intent.getStringExtra(Constants.last_name)
        val age = intent.getStringExtra(Constants.age)
        text.text = "Hello $name, I now that your last name is $last_name, and your age is $age"
    }
}