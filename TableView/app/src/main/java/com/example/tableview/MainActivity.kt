package com.example.tableview

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlin.random.*

private var MySpecCounter = 0
private var gachList: Array<Int> = arrayOf(
    R.drawable.gach1,
    R.drawable.gach2,
    R.drawable.gach3,
    R.drawable.gach4,
    R.drawable.gach5
)

class MainActivity : AppCompatActivity() {
    private lateinit var imageView: ImageView
    private lateinit var textView: TextView
    private lateinit var button: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button = findViewById(R.id.button)
        textView = findViewById(R.id.textField)
        imageView = findViewById(R.id.imageView)

        button.setOnClickListener {
            MySpecCounter = Random.nextInt(0, 5)
            textView.text = "$MySpecCounter"
            imageView.setImageResource(gachList[MySpecCounter])
//            val color: Int = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
//            yourView.setBackgroundColor(color)
//            button.setBackgroundColor()
        }
    }
}
