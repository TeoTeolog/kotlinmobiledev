import kotlinx.coroutines.*
import java.time.LocalDateTime

suspend fun main()= coroutineScope{
    //launch{  HelloWord() }
    launch{  pseudoMain()  }
    launch{  imSleeping()  }
    Unit
}

//Сделать программу, которая запускает в фоновом потоке метод печати «World» с задержкой в 1 сек.
// В основном потоке запускаем печать «Hello,» с задержкой в 2 сек., блокируем поток

suspend fun HelloWord()= coroutineScope{
    launch{
        delay(1000L)
        print("World")
    }

    println("Hello ")
    Thread.sleep(2000L)
}

// Написать две функции с задержкой, которые будут возвращать 2 числа.
// В main написать блок кода, который будет суммировать вызов этих 2 функций,
// сначала написать последовательный вызов функций, вывести сумму и время работы,
//  потом написать блок с асинхронным вызовом и сравнить время работы, обосновать время

suspend fun getNumber1(): Int{
    delay(1000L)
    return 10
}

suspend fun getNumber2(): Int{
    delay(2000L)
    return 8
}


suspend fun pseudoMain()= coroutineScope{
    var time = System.currentTimeMillis();

    val a: Int
    val b: Int
    a = getNumber1()
    b = getNumber2()
    println("first res: ${a+b}, time:${System.currentTimeMillis() - time}") //3011, потому что мы последовательно выполняем "тяжелые" операции

    time = System.currentTimeMillis()
    val a1: Deferred<Int> = async{ getNumber1()}
    val b1: Deferred<Int> = async{ getNumber2()}
    println("second res ${a1.await()+b1.await()}, time:${System.currentTimeMillis() - time}") //2003, потому что мы запускаем "тяжелые" операции асинхронно
}



// Дополнить код, чтоб программа выводила след текст в консоль.
// «I'm sleeping 0 ...I'm sleeping 1 ... I'm sleeping 2 ... main: I'm tired of waiting! I'm running finally main: Now I can quit.»


suspend fun imSleeping()= coroutineScope{
    launch{
        for(i in 0..2){
            println("I'm sleeping $i ...")
            delay(400L)
        }
    }

    Thread.sleep(1300L)
    println("main: I'm tired of waiting! I'm running finally main: Now I can quit.")

}